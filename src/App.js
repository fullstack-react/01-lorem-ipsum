import React from 'react';
import Hello from './components/hello'
import LoremIpsum from './components/lorem_ipsum'

function App() {
  return (
    <div className="App">
      <Hello></Hello>
      <LoremIpsum></LoremIpsum>
    </div>
  );
}

export default App;
